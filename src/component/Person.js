let Person = (props) => {
  let peopleObj = props.name;

  return (
    <div className="per-person">
      <img src={peopleObj.image} alt={peopleObj.name} />
      <p>{peopleObj.name}</p>
      <section>{peopleObj.description}</section>
      <button className="anchors">
        <a href={peopleObj.wikilink}>Know More!</a>
      </button>
    </div>
  );
};

export default Person;
