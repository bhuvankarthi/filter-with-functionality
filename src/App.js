import logo from "./logo.svg";
import "./App.css";
import React, { useEffect, useState } from "react";
import got from "./data";
import Person from "./component/Person";

const App = () => {
  const [names, setname] = useState([]);
  const [people, setpeople] = useState([]);
  const [searchText, setSearchText] = useState("");

  useEffect(() => {
    let namesArray = [];
    got.houses.map((el) => {
      namesArray.push(el.name);
    });
    setname(namesArray);

    let peopleArray = [];
    got.houses.map((fam) => {
      fam.people.map((person) => peopleArray.push(person));
    });
    setpeople(peopleArray);
  }, []);

  let initialChange = () => {
    let peopleArray = [];
    got.houses.map((fam) => {
      fam.people.map((person) => peopleArray.push(person));
    });
    setpeople(peopleArray);
  };

  let clickHandleChange = (name) => {
    got.houses.filter((family) => {
      if (family.name === name) {
        setpeople(family.people);
      }
    });
  };

  useEffect(() => {
    let people = [];
    got.houses.map((family) => {
      family.people.map((person) => {
        if (person.name.toLowerCase().includes(searchText.toLowerCase())) {
          people.push(person);
        }
      });
    });
    setpeople(people);
  }, [searchText]);

  return (
    <div className="App">
      <div className="heading-section">
        <h1>People Of Got</h1>
        <input
          value={searchText}
          onClick={() => {
            initialChange();
          }}
          onChange={(e) => {
            setSearchText(e.target.value);
          }}
        />
      </div>

      <div className="family-buttons">
        {names.map((name) => (
          <button className="namelist" onClick={() => clickHandleChange(name)}>
            {name}
          </button>
        ))}
      </div>
      <div className="total-people">
        {people.map((peopleObj) => {
          return <Person name={peopleObj} />;
        })}
      </div>
    </div>
  );
};

export default App;
